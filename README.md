# README
This is the code sample for the comprehensive tutorial [How to test thread safety in Java with TestNG](https://brokenrhythm.blog/howto-test-thread-safety-java-testng).

The code demonstrates how to test the thread safety of java code, using TestNG, Gradle and AssertJ.

## How to install
Clone this repository:

```
$ git clone https://bitbucket.org/apostolosbenisis/thread-safety-with-testng.git
```

Build with **gradle**:

```
$ cd thread-safety-with-testng
$ ./gradlew build
```

##  How to run tests
Start the tests with:
```
$ ./gradlew test
```

### Who do I talk to? ###
If you have questions leave a comment at the tutorial [blog article](https://brokenrhythm.blog/howto-test-thread-safety-java-testng).

You can also reach me at my [LinkedIn profile](https://www.linkedin.com/in/apostolos-benisis).
