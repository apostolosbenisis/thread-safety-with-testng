package threadsafe;

import java.util.LinkedList;

public class FooThreadsafeService {
    /**
     * Use a linked list to store the integer numbers.
     */
    private LinkedList<Integer> list = new LinkedList<>();

    /**
     * Get the last number from the list, increment it by 1 and add the new number at the end of the list.
     * If the list is empty add 0 as the first element. After calling this method N times,
     * the list is expected to have all the numbers from 0 to N-1 in ascending order, every number in
     * the list should appear only once and the list should not not contain any <tt>null</tt> entries.
     */
    public synchronized void increment() {
        int nextNumber = 0;
        int size = list.size();
        if (size > 0) {
            nextNumber = list.getLast() + 1;
        }
        Integer nextInteger = new Integer(nextNumber);
        list.addLast(nextInteger);

    }

    /**
     * Returns an int[] array with the entries of the list.
     **/
    public synchronized int[] getArray() {
        int[] intArray = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            intArray[i] = list.get(i);
        }
        return intArray;
    }
}
