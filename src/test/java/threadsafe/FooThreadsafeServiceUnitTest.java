package threadsafe;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class FooThreadsafeServiceUnitTest {
    //The instance of the service used in the tests.
    FooThreadsafeService fooService = new FooThreadsafeService();

    public final static int INVOCATION_COUNT = 10;

    @Test(threadPoolSize = 2, invocationCount = INVOCATION_COUNT)
    public void ConcurrentlyInvokingMethodsIsSuccessfull() throws Exception {
        //Calling the increment() or getArray() method, while the list is being concurrently modified by other threads,
        //must not throw any exceptions and the list's integrity must not be compromised.
        fooService.increment();
        listIntegrityNotCompromised(fooService.getArray());
    }

    private void listIntegrityNotCompromised(int[] intArray) {
        //The list is not empty.
        assertThat(intArray).isNotEmpty();
        //Elements of the list are in an continuous ordered sequence and it does not contain any nulls.
        for (int i = 0; i < intArray.length; i++) {
            assertThat(intArray[i]).isEqualTo(i);
        }
    }

    @Test(alwaysRun = true, dependsOnMethods = {"ConcurrentlyInvokingMethodsIsSuccessfull"})
    public void IncrementMethodInvocationCountEqualsToListSize() {
        int[] intArray = fooService.getArray();
        //Verify the integrity of the list.
        listIntegrityNotCompromised(intArray);
        //The list contains as many elements as the number of times the increment method was called.
        assertThat(fooService.getArray().length).isEqualTo(INVOCATION_COUNT);
    }
}
